﻿/// This is just a small recursive script file to recursively auto-run races
/// and optimize unknown factors of environment
 
#r "FSharp.Data.dll"

open System
open System.IO
open System.Net.Sockets
open FSharp.Data
open System.Threading.Tasks

#load "bot.fs"
#load "races.fsx"
open Bot
open Races

let rnd = System.Random()

type OptimizationType = Race | Qualify | Both
type SetupsSelection = 
| Separate of (string*CarSetup*(int*int)) []
| OneForAll of CarSetup*((string*(int*int)) [])

let raceWithSetup t c s = let r = ``race with given setup`` (Some s) (CreateRace(t,c)) [|"prost.helloworldopen.com"; "8091"; "Klosuuri"; "DiKXdgbixrRBzA"|] in r.``Track Id``, r.``Fastest lap``, r.``Race time``

/// All tracks and setups...
let separateSetups = 
        let tracks = [|"keimola"; "germany"; "usa"; "france"; "elaeintarha"; |]
        let setups = [|KeimolaRaceSetup; GermanyRaceSetup; UsaRaceSetup; FranceRaceSetup; ElainRaceSetup|]
        let ``lap time, race time`` = [|7350, 23084; 10217, 30934; 4916, 16250; 7484, 24084; 5950, 18800|]
        Array.zip3 tracks setups ``lap time, race time`` |> Separate

let separateNewSetups = 
        let tracks = [|"suzuka"; "imola"; "england";|]
        let setups = [|DefaultRaceSetup; DefaultRaceSetup; DefaultRaceSetup; |]
        let ``lap time, race time`` = [|10200, 31467; 8766, 88550; 10833, 109150; |]
        Array.zip3 tracks setups ``lap time, race time`` |> Separate

let oneForAllSetup =
        let tracks = [|"keimola"; "germany"; "usa"; "france"; "elaeintarha";|]
        let ``lap time, race time`` = [|7066, 22300; 10184, 30917; 5016, 16450; 7650, 24517; 6233, 19700|]
        OneForAll(DefaultRaceSetup, Array.zip tracks ``lap time, race time``)

let oneTrackOnly =
        let tracks = [|"elaeintarha"; |]
        let ``lap time, race time`` = [|5950, 18800|]
        OneForAll(ElainRaceSetup, Array.zip tracks ``lap time, race time``)


/// Optimization iterations like in a genetic algorithms:
let makeModifier x = 
    match rnd.Next(200) > 175 with
    | false -> Math.Round(1m / (1m+decimal(Math.Truncate(decimal(x)/15m))),2)
    | true -> Math.Round(decimal(rnd.Next(20)+1))
//To test: [1..60] |> List.map makeModifier;;

/// Make new config with slight random modification to current best
let adjustSetup triedCount initialConfig = 
    let modifier = makeModifier triedCount
    let fineTuneVariable() = (decimal(rnd.Next(int(modifier*20m)))/10m-(1m*decimal(modifier)))/20m        
    let acf = initialConfig.``allowed centrifugal force when curve exit``;
    let newSetup = {
            ``track grip factor`` = initialConfig.``track grip factor``+fineTuneVariable()
            ``allowed centrifugal force when curve exit`` = acf+fineTuneVariable()
            ``max corner out throttle`` = initialConfig.``max corner out throttle``+fineTuneVariable()
        }
    newSetup


/// Creates racing with n cars, each running in own async thread
let createRacing (tracksetups:(string*CarSetup)[]) =
    tracksetups |> Seq.map (fun (track,setup) -> fun _ -> raceWithSetup track 1 setup)
    |> runRacesAsParalllel


/// Recursively find the best configs
let findConfigs (optType:OptimizationType) (setupOptType:SetupsSelection) =
                                     
    let rec findConfigsRec tryCount (initSetupOptions:SetupsSelection) =
        printfn "Try %i" tryCount
        ``not hammer server``()
        if tryCount > 1000 then initSetupOptions
        else
        match initSetupOptions with
        | OneForAll(setup,tracktimes) ->
            let tolerance = match tracktimes.Length with | 1 -> 1 | _ -> 100 //ms
            printfn "Current: \r\n%A \r\ntracktimes: %A" setup tracktimes
            let newSetup = adjustSetup tryCount setup
            let newTrackTimes =
                tracktimes |> Array.map (fun (trackname,_) -> trackname,newSetup) |> createRacing
            let newIsBetter = tracktimes |> Array.forall(fun (tname,(lap,race)) -> 
                newTrackTimes |> Array.exists(fun (ntn,nlt,nrt) -> 
                    tname.ToUpper()=ntn.ToUpper() 
                    && match optType with 
                       | Both -> nlt-tolerance<=lap && nrt-tolerance<=race
                       | Qualify -> nlt<=lap+tolerance 
                       | Race -> nrt<=race+tolerance))
            if newIsBetter then 
                printfn "New record: %A" newSetup
                printfn "times: \n %A" newTrackTimes

                let newSetupOptions = OneForAll(newSetup, newTrackTimes |> Array.map (fun (x,y,z)->x.ToLower(),(y,z)))
                findConfigsRec (tryCount+1) newSetupOptions
            else findConfigsRec (tryCount+1) initSetupOptions
        | Separate(setupTracktimes) -> 
            if tryCount % 5 = 0 then
                printfn "Current tracktimes: \r\n%A" setupTracktimes
            let newSetups = setupTracktimes |> Array.map(fun (trackname, setup, (lap,race)) ->trackname, adjustSetup tryCount setup)
            let newTrackTimes = newSetups |> createRacing
            let nextGeneration = setupTracktimes |> Array.map(fun (tname,setup,(lap,race)) -> 
                match newTrackTimes |> Array.exists(fun (ntn,nlt,nrt) -> 
                    tname.ToUpper()=ntn.ToUpper() 
                    && match optType with 
                       | Both -> nlt<=lap && nrt<=race
                       | Qualify -> nlt<=lap 
                       | Race -> nrt<=race) with
                | true -> 
                    let selectCorresp = Array.tryFind(fun (t:string,_) -> t.ToUpper()=tname.ToUpper())
                    let selectCorresp2 = Array.tryFind(fun (t:string,_,_) -> t.ToUpper()=tname.ToUpper()) 
                    match newSetups |> selectCorresp, newTrackTimes |> selectCorresp2 with
                    |Some(newSetup), Some(newTime) -> 
                        printfn "New record, %s : %A" tname newSetup
                        let _,lapt,racet = newTime
                        tname,snd(newSetup),(lapt,racet)
                    | _ -> tname,setup,(lap,race)
                | false -> tname,setup,(lap,race))

            findConfigsRec (tryCount+1) (nextGeneration |> Separate)
           
    findConfigsRec 0 setupOptType

/// Try to find/create a new one-for-all config
let findConfigsOneNow() = oneForAllSetup |> findConfigs Both

/// Try to find/create all configs
let findConfigsAllNow() = separateSetups |> findConfigs Both
let findConfigsNewNow() = separateNewSetups |> findConfigs Both

/// Try to find/create all configs
let findConfigsSingleTrackNow() = oneTrackOnly |> findConfigs Both

/// Race all tracks with differents setups to select best for default
let compareSetupsOnAllTracks() =
    let tracks = [|"keimola",(0,0); "germany",(0,0); "usa",(0,0); "france",(0,0); "elaeintarha",(0,0)|]
    let runRace newSetup = tracks |> Array.map (fun (trackname,_) -> trackname,newSetup) |> createRacing
    let res = 
        ["default", runRace DefaultRaceSetup;
         "germany", runRace GermanyRaceSetup;
         "keimola", runRace KeimolaRaceSetup;
         "usa", runRace UsaRaceSetup;
         "france", runRace FranceRaceSetup;]
    printfn "%A" res
    let sums = res |> List.map(fun (t,r)-> t,r |> Array.sumBy(fun (t,_,s) -> s))
    printfn "%A" sums
    ()
