﻿/// Script-file to create some demo / test races

#r "FSharp.Data.dll"

open System
open System.IO
open System.Net.Sockets
open FSharp.Data
open System.Threading.Tasks
open Microsoft.FSharp.Data.UnitSystems.SI.UnitNames

#load "bot.fs"
open Bot
let rnd = System.Random()

/// Small random sleep
let ``not hammer server``() = System.Threading.Thread.Sleep(1000+rnd.Next(1000))

/// Creates a new race with track and cars
let createRace s t c = let r = race (CreateRace(t,c)) [|s+".helloworldopen.com"; "8091"; "Klosuuri_c"+rnd.Next(1000).ToString(); "DiKXdgbixrRBzA"|] in r.``Track Id``, r.``Fastest lap``, r.``Race time``
/// Joins a new race with track and cars
let joinRace s t c = let r = race (JoinRace(t,c)) [|s+".helloworldopen.com"; "8091"; "Klosuuri_j"+rnd.Next(1000).ToString(); "DiKXdgbixrRBzA"|] in r.``Track Id``, r.``Fastest lap``, r.``Race time``

/// Creates race-task as a background-thread task
let runTrack (ig:unit -> string*int*int) = 
    let twice = false
    Task.Run(fun _ -> 
        let dt1 = DateTime.Now.Ticks
        let name, lap, total = ig()
        let result = 
            if twice then 
                let name2, lap2, total2 = ig()
                name, int ((decimal (lap+lap2))/2m), int((decimal (total+total2))/2m)
            else
                name, lap, total
        printfn "%s took physically: %f sec " name (TimeSpan.FromTicks(DateTime.Now.Ticks-dt1).TotalSeconds)
        result
        ) |> Async.AwaitTask 

let runRacesAsParalllel = Seq.map runTrack >> Async.Parallel >> Async.RunSynchronously

/// Creates racing with n cars, each running in own async thread
let createRacing cars track =
    let svr = "testserver"
    [| (fun _ -> createRace svr track cars) |] 
    |> Seq.append ( [|2..cars|] |> Seq.map (fun _ -> (fun _ -> ``not hammer server``(); joinRace svr track cars)))
    |> runRacesAsParalllel

let server = "prost" // "testserver" "hakkinen" "senna" "webber" "prost"

/// Some quick races:
let raceNowUsa() = createRace server "usa" 1
let raceNowGermany() = createRace server "germany" 1
let raceNowKeimola() = createRace server "keimola" 1
let raceNowFrance() = createRace server "france" 1
let raceNowItaly() = createRace server "imola" 1
let raceNowEngland() = createRace server "england" 1
let raceNowJapan() = createRace server "suzuka" 1
//raceNowKeimola();raceNowGermany()

let raceNowElaintarha() = createRace server "elaeintarha" 1


/// Run each track simultaneously in parallel
let raceNowAll() = [| raceNowKeimola; raceNowUsa; raceNowGermany; raceNowFrance; |] 
                   |> runRacesAsParalllel

let raceNewNowAll() = [| raceNowElaintarha; raceNowItaly; raceNowEngland; raceNowJapan; |] 
                   |> runRacesAsParalllel

/// Racing with 4 cars!
let race8Germany() = createRacing 8 "germany"
let race4Keimola() = createRacing 4 "keimola"
let race4France() = createRacing 4 "france"
let race3Usa() = createRacing 6 "usa"
// race8Germany(); race4Keimola(); race4France(); race3Usa()

let race8Elaintarha() = createRacing 8 "elaeintarha"
let race5Italy() = createRacing 5 "imola"
let race6England() = createRacing 6 "england"
let race5Japan() = createRacing 5 "suzuka"

let race2Usa() = createRacing 2 "usa"

let joinRaceNow() = joinRace "testserver" "keimola" 2

let huntForRaces() =
    let rec huntForRacesLoop acc :unit =
        let ``concurrent searches`` = 4
        let server = "testserver"
        let cars() = rnd.Next(5)+3
        let track() = match rnd.Next(4) with | 0 -> "germany" | 1 -> "keimola" | 2 -> "france" | _ -> "usa"
        let res = [|1..``concurrent searches``|] |> Seq.map (fun _ -> fun _ -> ``not hammer server``(); joinRace server (track()) (cars())) |> runRacesAsParalllel |> Seq.toArray
        huntForRacesLoop res
    huntForRacesLoop [||]
    