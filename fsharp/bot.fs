//#if INTERACTIVE
//#r "FSharp.Data.Dll"
//#else
module Bot //Don't send this line to interactive
//#endif

open System
open System.IO
open System.Net.Sockets
open FSharp.Data

// -------------- CAR SETUPS FOR DIFFERENT TRACKS ------------------------------------------

type CarSetup = 
    {
        // When lost control:
        // If car is not able to turn enough, reduce grip.
        // If car is spinning out of corner, reduce allowed centrifugal force.

        // General force allowed to enter and drive in curve
        // Scaled to nice values...
        ``track grip factor`` : decimal;
        // The next two settings:
        // Analog throttle while centrifugal force:
        // - Don't throttle ower this value when exit a curve.
        // The reason for this is to avoid digital-on-off-throttle-bump when exiting curve
        ``allowed centrifugal force when curve exit`` : decimal;
        ``max corner out throttle`` : decimal;
    }

// Default setup 25.4.2014 laptimes (may vary) for all tracks:
//// Without turbo:  [|("Keimola", 8083, 24900); ("Germany", 10317, 31500); ("USA", 5250, 16617); ("France", 7817, 24184)|]
//// With turbo:     [|("Keimola", 7066, 22300); ("Germany", 10184, 30917); ("USA", 5016, 16450); ("France", 7650, 24517); ("Elaeintarhan ajot", 6233, 19700); ("Suzuka", 10200, 31467); ("England", 10833, 109150); ("Imola", 8766, 88550); |]
let DefaultRaceSetup = { ``track grip factor`` = 0.1600m; ``allowed centrifugal force when curve exit`` = 0.9700m; ``max corner out throttle`` = 0.7600m } //7100, 22217
let GermanyRaceSetup = { ``track grip factor`` = 0.2050m; ``allowed centrifugal force when curve exit`` = 0.6550m; ``max corner out throttle`` = 0.5750m } //9466, 29100
let KeimolaRaceSetup = { ``track grip factor`` = 0.1600m; ``allowed centrifugal force when curve exit`` = 0.9700m; ``max corner out throttle`` = 0.7600m } //7100, 22217
let UsaRaceSetup =     { ``track grip factor`` = 0.5200m; ``allowed centrifugal force when curve exit`` = 0.7450m; ``max corner out throttle`` = 0.7450m } //4567, 15084
let FranceRaceSetup =  { ``track grip factor`` = 0.2050m; ``allowed centrifugal force when curve exit`` = 0.7500m; ``max corner out throttle`` = 0.0150m } //7600, 24350
let ElainRaceSetup =   { ``track grip factor`` = 0.2950m; ``allowed centrifugal force when curve exit`` = 0.8100m; ``max corner out throttle`` = 0.0550m } //5950, 18800
let ImolaRaceSetup =   { ``track grip factor`` = 0.2050m; ``allowed centrifugal force when curve exit`` = 0.7800m; ``max corner out throttle`` = 0.0600m } //8600, 87167
let SuzukaRaceSetup =  { ``track grip factor`` = 0.1800m; ``allowed centrifugal force when curve exit`` = 0.6750m; ``max corner out throttle`` = 0.5300m } //10217, 104334
let EnglandRaceSetup = { ``track grip factor`` = 0.1600m; ``allowed centrifugal force when curve exit`` = 0.9700m; ``max corner out throttle`` = 0.7600m } //10833, 109150

let ``get car setup by name`` = function
    | "keimola" -> KeimolaRaceSetup
    | "germany" -> GermanyRaceSetup
    | "usa" -> UsaRaceSetup
    | "france" -> FranceRaceSetup
    | "elaeintarha" -> ElainRaceSetup
    | "imola" -> ImolaRaceSetup
    | "suzuka" -> SuzukaRaceSetup
    | "england" -> EnglandRaceSetup
    | _ -> DefaultRaceSetup

//These are basic constants that should be ok as is:
let ``Turbo allowed main switch`` = true
let ``grip constant`` = 18m // Smaller value means slower speed in corners
let ``agressiveness factor`` = 0.75m // Smaller value means easier turbo use behind an opponent 
let maxRadius = 800 // Max radius of any curve
let maxTime = 999999 // Max race time
let fullThrottle = 1.0m
let rnd = System.Random()
let ``max previous lap angle`` = 55.0
let ``min previous lap angle`` = 50.0
// -------------- DATA TYPES ------------------------------------------

type JsonSend = JsonProvider<"./sample.json", SampleIsList=true, RootName="message">
type JsonGet = JsonProvider<"./hocken.json", SampleIsList=true, RootName="message">

type GameType = 
| QuickRace
| CreateRace of string*int //trackname*cars
| JoinRace of string*int //trackname*cars

type LaneSwitch = Left | Right
type ``Lane change`` =
| WantSwitch of LaneSwitch*int * int // int*int: to-Lane-index * Switch-piece-index
| HasSwitched of LaneSwitch * int * int  // int*int: to-Lane-index * Switch-piece-index
| WaitForNextSwitch of int*int  //Lane-index * Switch-piece-index 
| DontChange
| SwitchInitialState


/// The game state
[<NoComparison>]
type InGameData = {
    ``Track Id``: string
    ``Fastest lap``: int
    ``Race time``: int
    //pieces, min-radius, angles total, clockwise:
    ``Track shape``: JsonGet.Piecis array*int*decimal*bool
    ``Lanes``: JsonGet.Lane2 array
    ``Race on-going``: bool
    ``Car position``: JsonGet.Datum option
    ``Turbo available``: (int*int) list
    ``Laps``: int
    ``Lane change request``: ``Lane change``
    ``Car setup``:CarSetup
    ``Crash tick and count``:int*int //last crash, total count
    ``Max angle``:float
    ``Car size``:int
}

// -------------- LOGGING ------------------------------------------

let logPath = @"C:\Users\tuomashie\Documents\GitHub\hwo2014-team-921\fsharp\"
let logTicks = System.DateTime.Now.Ticks.ToString()

let log str = 
    /// This will log basically everything, 2 MB of force and analytics data per lap
    /// Used only to debug while one car in track...
    let useLog = false
    let file = logPath+"RaceLog"+logTicks+".log"
    if useLog then 
        try File.AppendAllText(file, str)
        with ex -> () // printfn "%s (log didn't work: %s)" str ex.Message 

let logResults str track = 
    /// This will log only setup and results: Used to fine-tune car on track setup...
    let useLog = false   
    let file = logPath+"RaceResults-"+track+".json"
    if useLog then 
        try File.AppendAllText(file, str)
        with ex -> printfn "%s (log didn't work: %s)" str ex.Message 

// -------------- COMMANDS TO SERVER ------------------------------------------

let stopTheRace (gameState:InGameData) = { gameState with Laps = 0; ``Race on-going``=false; }

let send (msg : JsonSend.Message) (writer : StreamWriter) =
    writer.WriteLine (msg.JsonValue.ToString(JsonSaveOptions.DisableFormatting))
    writer.Flush()

let fullRace act name key trackName carsCount =
    let racePwd = None //Some("myRacePwd")
    JsonSend.Message(msgType = act, gameTick=None, data = JsonSend.DecimalOrStringOrData(JsonSend.Data(None, None, None, Some(JsonSend.BotId(name, key)), Some(carsCount), racePwd, Some(trackName))))

let join name key = JsonSend.Message(msgType = "join", gameTick=None, data = JsonSend.DecimalOrStringOrData(JsonSend.Data(None, Some(key), Some(name), None, None, None, None)))
let switchLaneLeft tick = JsonSend.Message(msgType = "switchLane", gameTick=tick, data = JsonSend.DecimalOrStringOrData("Left"))
let switchLaneRight tick = JsonSend.Message(msgType = "switchLane", gameTick=tick, data = JsonSend.DecimalOrStringOrData("Right"))
let throttle tick (value : Decimal) = JsonSend.Message(msgType = "throttle", gameTick=tick, data = JsonSend.DecimalOrStringOrData(value))
let useTurbo tick (turboMsg:string) = JsonSend.Message(msgType = "turbo", gameTick=tick, data = JsonSend.DecimalOrStringOrData(turboMsg))
let ping = JsonSend.Message(msgType = "ping", gameTick=None, data = null)

let crerateRace = fullRace "createRace"
let joinRace = fullRace "joinRace" 

// -------------- THE CALCULATION OF THE THROTTLE ------------------------------------------

let ``analyse track profile`` (track:JsonGet.Piecis array) =
    let ``sharperst corner`` = track |> Seq.map(fun t -> match t.Radius with Some(x) -> x | None -> maxRadius) |> Seq.min
    let ``corners amount`` = track |> Seq.map(fun t -> match t.Angle with Some(x) -> abs(x) | None -> 0m) |> Seq.sum
    let ``drive to clockwise`` = track |> Seq.sumBy(fun p -> match p.Angle with Some(a) -> a | None -> 0m) > 0m
    ``sharperst corner``, ``corners amount``, ``drive to clockwise``

// Basic circle math:
let degRad = 2m*decimal(Math.PI)/360m
let cornerDiameter alpha radius = degRad*radius*abs(alpha)
let ``angular velocity`` d radius = d/(degRad*radius)

// F=m*a and F= m*(v^2)/r  ->  a = (v^2)/r
let ``centrifugal force`` (radius:decimal) velocity = 
    match velocity with
    | Some(v) -> 
        v*v/radius
    | _ -> 0m

// Breaking distance: s = (v^2) / 2*(myy)*g -> myy = (v^2) / 2*s*g
let ``required friction`` velocity s = 
    match velocity with Some(v) -> v*v/(2m * s * 9.80665m) | _ -> 0m

/// Fetch a track piece in position of x to current piece.
/// Track is modulo-calculus, like a clock, you can't fall off
let fetchRelativePosition (track:JsonGet.Piecis array) currentPosition idx =
    let pIdx = currentPosition + idx
    match pIdx < 0 with
    | false -> track.[pIdx % track.Length]
    | true when pIdx % track.Length = 0 -> track.[0]
    | true -> track.[track.Length + (pIdx % track.Length)]

/// To throttle or throttle, that's the question
let throttleCalculation (myCar:JsonGet.Datum) (otherCars:JsonGet.Datum array) setup (gameState:InGameData) (hasTurbo:(int*int) option) = 
    let lanes = gameState.Lanes
    let prevCarPosition = gameState.``Car position``
    let track,_,_,_ = gameState.``Track shape``
    let fetchRelativePiece = fetchRelativePosition track myCar.PiecePosition.PieceIndex 

    /// Pattern match a piece to straight or corner piece
    let (|Straight|Corner|) (piece:JsonGet.Piecis) =
        match piece.Angle, piece.Radius, piece.Length with
        | _, _, Some(i) ->
            Straight(decimal(i))
        | Some(a),Some(r),_ when r < maxRadius -> 
            let ``first piece next lap`` i = i > track.Length-1 && myCar.PiecePosition.PieceIndex = i-track.Length
            let ``going to change the lane`` = 
                match gameState.``Lane change request`` with
                | WantSwitch(_,l,i) | WantSwitch(_,l,i) -> Some(l)
                | HasSwitched(_,l,i)     when i > myCar.PiecePosition.PieceIndex || ``first piece next lap`` i -> Some(l)
                | WaitForNextSwitch(l,i) when i > myCar.PiecePosition.PieceIndex || ``first piece next lap`` i -> Some(l)
                | SwitchInitialState -> Some(myCar.PiecePosition.Lane.StartLaneIndex)
                | _ -> None

            let adjustedR =
                match ``going to change the lane`` with
                | None -> // Curve radius with lane position
                    let laneDistance = lanes.[myCar.PiecePosition.Lane.EndLaneIndex].DistanceFromCenter
                    if a>0m then r-laneDistance
                    else r+laneDistance
                | Some(wanted) -> 
                    // Lane-change just before curve, has to be careful: change-request may success or fail
                    let distances = [|
                        lanes.[myCar.PiecePosition.Lane.EndLaneIndex].DistanceFromCenter; 
                        lanes.[wanted].DistanceFromCenter |] 
                    if a>0m then r-(distances |> Array.max)
                    else r+(distances |> Array.min)
                    
            Corner(decimal(a),decimal(adjustedR))
        | _ -> Straight 0m

    /// Is this the home stretch? Then we can use all the remaining turbos...
    let isLastStraight() = 
        match gameState.Laps - myCar.PiecePosition.Lap = 1 with
        | false -> false
        | true ->
            let lastStraights = 
                track |> Array.rev 
                |> Seq.takeWhile(fun i -> match i with Straight(_) -> true | Corner(_) -> false)
                |> Seq.length
            myCar.PiecePosition.PieceIndex >= (track.Length - lastStraights)

    let isInFront checklap carDistance (car:JsonGet.Datum) = 
            car.Angle < 50.0 && myCar.Angle < 50.0 &&
            let carLane = car.PiecePosition.Lane
            let myLane = myCar.PiecePosition.Lane
            let sameLanes = carLane.StartLaneIndex = myLane.StartLaneIndex && carLane.EndLaneIndex = myLane.EndLaneIndex
            let ``opponent near distance`` = decimal(gameState.``Car size``)*carDistance
            let sameLap = not(checklap) || car.PiecePosition.Lap = myCar.PiecePosition.Lap
            match car.PiecePosition.PieceIndex, sameLap with
            | x, true when x = myCar.PiecePosition.PieceIndex ->
                let dist = car.PiecePosition.InPieceDistance - myCar.PiecePosition.InPieceDistance
                dist > 0m && dist < ``opponent near distance`` && sameLanes
            | x, true when x = myCar.PiecePosition.PieceIndex+1 ->
                myCar.PiecePosition.InPieceDistance - car.PiecePosition.InPieceDistance > ``opponent near distance`` && sameLanes
            | x, true when x = 0 && myCar.PiecePosition.PieceIndex = track.Length-1->
                myCar.PiecePosition.InPieceDistance - car.PiecePosition.InPieceDistance > ``opponent near distance`` && sameLanes
            | _ -> false

    let ``other car in my front`` = otherCars.Length > 0 && otherCars |> Array.exists(isInFront true 1.01m)
    let ``other cars in my front`` = otherCars |> Array.filter(fun c -> isInFront false 3m c) |> Array.length

    let currentPiece = fetchRelativePiece 0
    let nextPiece = fetchRelativePiece +1
    let next2Piece = fetchRelativePiece +2

    /// Current velocity of the car. Basically time is tick so velocity is just position change.
    let velocity = 
        match prevCarPosition with
        | None -> None
        | Some(p) when p.PiecePosition.PieceIndex <> myCar.PiecePosition.PieceIndex -> 
            let prevDistance =
                match (fetchRelativePiece -1) with
                | Straight i -> abs(i - p.PiecePosition.InPieceDistance)
                | Corner(a,r) -> abs((cornerDiameter a r) - p.PiecePosition.InPieceDistance)
            Some(prevDistance + myCar.PiecePosition.InPieceDistance)
        | Some(p) -> Some(abs(myCar.PiecePosition.InPieceDistance - p.PiecePosition.InPieceDistance))
    
    /// Current straight length to next corner, and centrifugal force of the corner: should we brake already?     
    let ``inside braking distance`` straightLengthLeft nextCornerRadius alpha =
        let ce = ``centrifugal force`` nextCornerRadius velocity        
        let rf = ``required friction`` velocity straightLengthLeft
        let ``weighted force`` = rf*ce*1000m
        let ``allowed force`` = nextCornerRadius*setup.``track grip factor``+``grip constant``
        let fastCurve = 150m

        let shouldBrake = match straightLengthLeft, ``other car in my front`` && nextCornerRadius < fastCurve with
                          | d, true when d > 1m -> ``weighted force``* (``agressiveness factor``+0.15m) >= ``allowed force``
                          | d, _ when d > 1m -> ``weighted force`` >= ``allowed force``
                          | d, _ -> ce > setup.``allowed centrifugal force when curve exit``
        let turboAllowed = 
            match isLastStraight(), hasTurbo, velocity with
            | true, Some(_), _ -> true
            | _, Some(dur, fact), Some(v) ->
                let turboFactorForce = ``weighted force``*decimal(fact)
                let turboLen = (v+1m)*decimal(fact)*decimal(dur)
                let agressiveness = match ``other cars in my front`` with x when x < 3 -> (decimal(x)*``agressiveness factor``+1m) | _ -> 4m*``agressiveness factor``+1m 
                
                let mayUse = turboLen < straightLengthLeft * agressiveness && 
                             match straightLengthLeft with
                             | d when d > 30m -> turboFactorForce < ``allowed force`` * agressiveness
                             | d -> ce*decimal(fact) > setup.``allowed centrifugal force when curve exit`` && nextCornerRadius > fastCurve
                 
                log ("\r\nTurbo params: w " + ``weighted force``.ToString() + " a " + ``allowed force``.ToString() + " sLen " + straightLengthLeft.ToString() + " d "+ dur.ToString() + " so: turboforce " + 
                            turboFactorForce.ToString() + " vs a, and " + turboLen.ToString())
                mayUse
            | _ -> false

        let currentOrNext = match straightLengthLeft <= 1m with |true -> "Current" | false -> "Next"
        
        log ("\r\n"+currentOrNext+" corner, rad:" + nextCornerRadius.ToString() + " distance " + straightLengthLeft.ToString() + " centrifugal " + ce.ToString() + " force " + ``weighted force``.ToString() + " should brake: " + shouldBrake.ToString() + " turbo: " + turboAllowed.ToString())
        shouldBrake, ce, turboAllowed

    /// Fetch recursively next (max 10) curvers: their distance and tightness
    let ``analyse curves ahead``() = 
        let rec ``distance and curve properties`` i acc foundRadius =
            let acclen, curves = acc
            let foundAlready = curves |> List.length
            match fetchRelativePiece i with
            | Straight(len) -> 
                ``distance and curve properties`` (i+1) (len+acclen, curves) foundRadius
            | Corner(a,nextRadius) -> 
                match nextRadius<=foundRadius with
                | true when foundAlready < 10 -> 
                    let thisLen = cornerDiameter a nextRadius
                    let correctedAccLen = match acclen<1m with true -> 1m | false -> acclen
                    let shouldBreak, centrifugalForce, turboAllowed = ``inside braking distance`` correctedAccLen nextRadius a
                    ``distance and curve properties`` (i+1) (acclen+thisLen, (correctedAccLen,shouldBreak,centrifugalForce,turboAllowed)::curves) nextRadius
                | _ -> curves
        ``distance and curve properties`` 0 (-myCar.PiecePosition.InPieceDistance, []) (decimal maxRadius)
    
    let ``analog curve exit throttling`` ``corner radius`` =
        let ce = ``centrifugal force`` ``corner radius`` velocity
        log("\r\ncentrifugal force: " + ce.ToString())
        if ce > setup.``allowed centrifugal force when curve exit`` then
            setup.``max corner out throttle``-ce, None
        else fullThrottle, None

    let shouldBrakeNow, couldUseTurbo = 
        let curves = ``analyse curves ahead``()
        //curves |> List.iter (fun (dist, sb, ce, tu) -> log ( "\r\nNext curve, distance: " + dist.ToString() + " should break: " + sb.ToString() + " centrifugal force: " + ce.ToString() + " turboOk " + tu.ToString()))
        let shouldBrake = curves |> List.exists (fun (dist,sb,_,_) -> sb=true)
        let turboOk = curves |> List.exists (fun (_,_,_,tu) -> tu=false) |> not
        let turboMsg = match turboOk, rnd.Next(3) with |false,_ -> None |_,0 -> Some("F(y) = y(F(y))") |_,1 -> Some("s x y z = x z (y z)") | _ -> Some("let (>>=) f g x = g(f(x))") 
        shouldBrake, 
        match currentPiece with
        | Straight(_) -> log ("\r\nStraight"); turboMsg
        | Corner(ca,cr) -> log (match nextPiece with Straight(_) ->"\r\nCorner-Straight" | Corner(_,_) -> "\r\nCorner-Corner"); None
    

    let speed,turboAllow =
        match shouldBrakeNow with 
        | true -> 0.0m, None 
        | false -> match currentPiece with Straight(_) -> fullThrottle, couldUseTurbo | Corner(ca,cr) -> ``analog curve exit throttling`` cr

    let ``limited speed`` = 
        if speed < 0.0001m then 0.0001m elif speed > fullThrottle then fullThrottle else speed
        
    ``limited speed``, turboAllow

// -------------- THE CALCULATION OF THE ROUTE ------------------------------------------

let ``take the best lane`` (myCar:JsonGet.Datum) (otherCars:JsonGet.Datum array) (gameState:InGameData) = 
    let track,_,_,``drive to clockwise`` = gameState.``Track shape``
    let lastPieceIdx = track.Length-1
    let myPositionIdx = myCar.PiecePosition.PieceIndex
    let fetchRelativePiece = fetchRelativePosition track myPositionIdx
    let ``opponent near distance`` = decimal(gameState.``Car size``)*3.0m
    let lanes = gameState.Lanes
    let laneCount = lanes.Length
    let mostLeftLane = lanes |> Seq.minBy(fun p -> p.DistanceFromCenter)
    let mostRightLane = lanes |> Seq.maxBy(fun p -> p.DistanceFromCenter)


    /// The track-sector distance from this switch-lane-point to the next switch-lane-point:
    /// pattern match if the track is bending to left or right 
    let (|BendLeft|NoBend|BendRight|) idx =    
        let ``non-curve degrees``=5m
        let rec ``sum angle`` i acc = 

            let ``final stretch but laps left``() = 
                let tlen = (track.Length-1)
                i < tlen && acc > -``non-curve degrees`` && acc < ``non-curve degrees`` &&
                myCar.PiecePosition.PieceIndex + idx < tlen && 
                myCar.PiecePosition.PieceIndex + i > tlen &&
                gameState.Laps - myCar.PiecePosition.Lap <> 1

            let nextPiece = fetchRelativePiece i
            match nextPiece.Angle, nextPiece.Switch with
            | Some(a), Some(true) when ``final stretch but laps left``() -> ``sum angle`` (i+1) (a+acc)
            | _, Some(true) when ``final stretch but laps left``() -> ``sum angle`` (i+1) acc
            | _, Some(true) -> acc
            | Some(a), _ ->  ``sum angle`` (i+1) (a+acc)
            | _  -> if i < track.Length*2 then ``sum angle`` (i+1) acc else 0m

        match ``sum angle`` idx 0m with 
        | a when a < -``non-curve degrees`` -> BendLeft (abs a)
        | a when a > ``non-curve degrees`` -> BendRight (abs a)
        | a -> NoBend
    
    let occupyLane checkIfLapped checkDistance lane (car:JsonGet.Datum) = 
        (not(checkIfLapped) || car.PiecePosition.Lap < myCar.PiecePosition.Lap) &&
        car.PiecePosition.Lane.EndLaneIndex = lane &&
        match car.PiecePosition.PieceIndex with
        | x when x = myPositionIdx -> 
            let pos = car.PiecePosition.InPieceDistance - myCar.PiecePosition.InPieceDistance
            pos > 0m && pos < checkDistance
        | x when x = myPositionIdx+1 || (x = 0 && myPositionIdx = lastPieceIdx) -> 
            myCar.PiecePosition.InPieceDistance - car.PiecePosition.InPieceDistance > checkDistance
        | _ -> false
    let ``lane is occupied`` lane = otherCars.Length>0 && otherCars |> Array.exists (occupyLane false ``opponent near distance`` lane)
    let ``lane is occupied by lapped opponent`` lane = otherCars.Length>0 && otherCars |> Array.exists (occupyLane true ``opponent near distance`` lane)
    let ``traffic jam cars`` lane = otherCars |> Array.filter (occupyLane false (``opponent near distance``*2m) lane) |> Array.length

    let leftSideLane = myCar.PiecePosition.Lane.EndLaneIndex - 1
    let availableLeft = leftSideLane>=0 && not(``lane is occupied``(leftSideLane))
    let rightSideLane = myCar.PiecePosition.Lane.EndLaneIndex + 1
    let availableRight = rightSideLane<=(laneCount-1) && not(``lane is occupied``(rightSideLane))

    /// When the lane is not bending, then there is no clear best possible lane. So we have options...
    let ``select lane on straight`` idx =
        let trackposition = match idx + myPositionIdx with
                            | x when x > lastPieceIdx -> x - track.Length
                            | x -> x
        let ``someone is just behind`` = otherCars.Length>0 && otherCars |> Array.exists (fun car -> 
            match car.PiecePosition.PieceIndex with
            | x when x = myPositionIdx -> 
                let pos = myCar.PiecePosition.InPieceDistance - car.PiecePosition.InPieceDistance
                pos > 0m && pos < ``opponent near distance``
            | x when x = myPositionIdx-1 || (x = lastPieceIdx && myPositionIdx = 0) -> 
                car.PiecePosition.InPieceDistance - myCar.PiecePosition.InPieceDistance > ``opponent near distance``
            | _ -> false)

        let selectAvailable ``cars infront`` =
            match availableLeft, availableRight with
            | false, false -> 
                // Check for traffic jams:
                match (``traffic jam cars`` leftSideLane), (``traffic jam cars`` rightSideLane), ``cars infront`` with
                | l,r,c when l > 0 && r > 0 && r < c && r < l -> WantSwitch(Right, rightSideLane, trackposition)
                | l,r,c when l > 0 && l < c-> WantSwitch(Left, leftSideLane, trackposition)
                | l,r,c when r > 0 && r < c-> WantSwitch(Right, rightSideLane, trackposition)
                | _ -> DontChange
            | false, true -> WantSwitch(Right, rightSideLane, trackposition)
            | true, false -> WantSwitch(Left, leftSideLane, trackposition)
            | true, true -> match ``drive to clockwise`` with true -> WantSwitch(Right,rightSideLane,trackposition) | false -> WantSwitch(Left,leftSideLane,trackposition)

        match laneCount, ``someone is just behind``, ``traffic jam cars``(myCar.PiecePosition.Lane.EndLaneIndex) with
        // If there are over 2 lanes, we want to avoid the far-outer-edge-lanes 
        | lc, _, _ when lc > 2 && ``drive to clockwise`` && mostLeftLane.Index = myCar.PiecePosition.Lane.EndLaneIndex -> WantSwitch(Right,rightSideLane,trackposition)
        | lc, _, _ when lc > 2 && not(``drive to clockwise``) && mostRightLane.Index = myCar.PiecePosition.Lane.EndLaneIndex -> WantSwitch(Left,leftSideLane,trackposition)
        // There is on just behind us, this is dangerous if (s)he has also a turbo:
        | lc, true, traffic when traffic = 0 -> 
            if gameState.``Turbo available``.Length>0 && (availableLeft || availableRight) then selectAvailable(0)
            else DontChange
        | lc, false, traffic when traffic > 0 -> 
        // Ok, there is no-one just behind us, but someone is on our way:
            if gameState.``Turbo available``.Length>1 && traffic < 2 then DontChange else
            selectAvailable(traffic)
        | lc, _, traffic when traffic > 1 -> selectAvailable(traffic)
        | _ -> DontChange 

    let rec ``find lance switch`` idx =
        match (fetchRelativePiece idx).Switch with
        | Some(true) -> 
            match idx+1 with
            | NoBend -> ``select lane on straight`` idx
            | BendLeft a -> match mostLeftLane.Index = myCar.PiecePosition.Lane.EndLaneIndex with 
                            | true when a<90m && ``lane is occupied by lapped opponent`` myCar.PiecePosition.Lane.EndLaneIndex -> ``select lane on straight`` idx
                            | true -> DontChange
                            | false when ``traffic jam cars``(mostLeftLane.Index)>1 && not(``lane is occupied`` myCar.PiecePosition.Lane.EndLaneIndex) -> DontChange
                            | false when idx+myPositionIdx > lastPieceIdx -> 
                                       WantSwitch(Left,leftSideLane, idx + myPositionIdx - track.Length)
                            | false -> WantSwitch(Left,leftSideLane, idx + myPositionIdx)
            | BendRight a -> match mostRightLane.Index = myCar.PiecePosition.Lane.EndLaneIndex with 
                             | true when a<90m && ``lane is occupied by lapped opponent`` myCar.PiecePosition.Lane.EndLaneIndex -> ``select lane on straight`` idx
                             | true -> DontChange 
                             | false when ``traffic jam cars``(mostRightLane.Index)>1 && not(``lane is occupied`` myCar.PiecePosition.Lane.EndLaneIndex) -> DontChange
                             | false when idx+myPositionIdx > lastPieceIdx -> 
                                        WantSwitch(Right,rightSideLane, idx + myPositionIdx - track.Length)
                             | false -> WantSwitch(Right,rightSideLane, idx + myPositionIdx)
        | _ -> 
            if idx+myPositionIdx <= lastPieceIdx then ``find lance switch`` (idx+1)
            elif idx+myPositionIdx > lastPieceIdx then ``find lance switch`` (idx+1-track.Length)
            else DontChange

    // Lane change decided a bit too early: to be able to brake also to next corner
    let fwdCalcPieceAmount = 1
    
    let ``switch is very near`` = 
        [0..fwdCalcPieceAmount] 
        |> List.map fetchRelativePiece 
        |> List.exists(fun t -> t.Switch.IsSome && t.Switch.Value = true)
    
    let ``jammed in traffic``() = 
        myCar.Angle < 1.0 && gameState.``Car position``.IsSome &&
        myPositionIdx = gameState.``Car position``.Value.PiecePosition.PieceIndex &&
        myCar.PiecePosition.InPieceDistance < (gameState.``Car position``.Value.PiecePosition.InPieceDistance+3m) &&
        ``traffic jam cars``(myCar.PiecePosition.Lane.EndLaneIndex) > 2

    match gameState.``Lane change request`` with 
    | DontChange when not(``switch is very near``) -> ``find lance switch`` (fwdCalcPieceAmount)
    | SwitchInitialState when myCar.PiecePosition.Lap = 0 && myPositionIdx >= 0 && myPositionIdx <= fwdCalcPieceAmount -> ``find lance switch`` 0
    | WaitForNextSwitch(_,i) when not(``switch is very near``) -> ``find lance switch`` (fwdCalcPieceAmount-1)
    | _ when ``jammed in traffic``() && gameState.Laps - myCar.PiecePosition.Lap > 1 -> ``find lance switch`` 0
    | _ -> gameState.``Lane change request``

// -------------- THE RACE COMMUNICATION ------------------------------------------

/// There are two ways to detect car setup:
/// 1) Auto-detect the setup (None-parameter)
/// 2) Manually force setup. This is mainly to optimization purposes, to find better setups.
let ``race with given setup`` setup gameType args =
    let (host, portString, botName, botKey) = 
        args |> (function [|a;b;c;d|] -> a, b, c, d | _ -> failwith "Invalid param array")
    let port = Convert.ToInt32(portString+"")
    let myCars = new System.Collections.Generic.List<_>()

    printfn "Connecting to %s:%d as %s/%s" host port botName botKey

    use client = new TcpClient(host, port)
    let stream = client.GetStream()
    use reader = new StreamReader(stream)
    use writer = new StreamWriter(stream)

    match gameType with
    | QuickRace -> send (join botName botKey) writer
    | CreateRace(trackname,carsCount) -> send (crerateRace botName botKey trackname carsCount) writer
    | JoinRace(trackname,carsCount) -> send (joinRace botName botKey trackname carsCount) writer

    let processNextLine nextLineAsync (gameState:InGameData) =
        async {
            let! nextLine = nextLineAsync
            match nextLine with
            | null -> 
                return stopTheRace gameState
            | line ->
                log (",\n")
                log line
                try 
                    let msg = JsonGet.Parse(line)                    
                    return 
                        match msg.MsgType with
                        | "carPositions" -> // In-game actions
                            let myCar = msg.Data.Array.Value |> Array.find (fun i -> myCars.Contains(i.Id.Name + "-" + i.Id.Color))
                            let maxAngle = match gameState.``Max angle`` <= myCar.Angle with true -> myCar.Angle | false -> gameState.``Max angle``
                            let otherCars = msg.Data.Array.Value |> Array.filter (fun car -> car.Id.Name <> myCar.Id.Name && car.Id.Color <> myCar.Id.Color)

                            let firstTurbo = match gameState.``Turbo available`` with [] -> None | h::t -> Some(h)

                            let speed, ``could use a turbo`` = throttleCalculation myCar otherCars gameState.``Car setup`` gameState firstTurbo
                            
                            let ``should change lane`` = ``take the best lane`` myCar otherCars gameState
                            let lastPiece = 
                                let track = gameState.``Track shape`` |> (fun (x,_,_,_) -> x) in track.Length-1
                            let laneChangeAction =
                                match ``should change lane`` with
                                // Want to switch, send command one piece before the switch:
                                | WantSwitch(w,l,i) when i-1 = myCar.PiecePosition.PieceIndex || (i=0 && myCar.PiecePosition.PieceIndex = lastPiece )-> None, HasSwitched(w,l,i+1)
                                // Then wait until switch is over and then wait for next switch:
                                | HasSwitched(w,l,i) -> Some(w), WaitForNextSwitch(l,i+1)
                                | a -> None, a

                            // Some special conditions:  
                            if fst(laneChangeAction)<>None then
                                match fst(laneChangeAction).Value with 
                                | Left -> 
                                    log ("Change lane request left")
                                    printfn ("Change lane request left")
                                    send (switchLaneLeft msg.GameTick) writer
                                | Right -> 
                                    log ("Change lane request right")
                                    printfn ("Change lane request right")
                                    send (switchLaneRight msg.GameTick) writer
                                {gameState with ``Car position`` = Some(myCar); ``Lane change request`` = snd(laneChangeAction); ``Max angle`` = maxAngle}
                            elif speed > 0.5m && ``Turbo allowed main switch`` && gameState.``Turbo available``.Length > 0 && ``could use a turbo``.IsSome then 
                                log ("\r\nUsed a turbo. ")
                                send (useTurbo msg.GameTick ``could use a turbo``.Value) writer
                                let restTurbos = gameState.``Turbo available``.Tail
                                {gameState with ``Car position`` = Some(myCar); ``Turbo available``=restTurbos; ``Lane change request`` = snd(laneChangeAction); ``Max angle`` = maxAngle}
                            else
                                log ("\r\nSent throttle: " + speed.ToString())
                                send (throttle msg.GameTick speed) writer
                                {gameState with ``Car position`` = Some(myCar); ``Lane change request`` = snd(laneChangeAction); ``Max angle`` = maxAngle}
                            
                        | "lapFinished" when msg.Data.Record.IsSome && msg.Data.Record.Value.Car.IsSome -> 
                            let car = msg.Data.Record.Value.Car.Value
                            let isMyLap = myCars.Contains(car.Name + "-" + car.Color)
                            let ``is last lap`` = msg.Data.Record.Value.RaceTime.IsSome && msg.Data.Record.Value.RaceTime.Value.Laps=gameState.Laps
                            if not(isMyLap) || ``is last lap`` then 
                                send (ping) writer; gameState
                            else
                                let ``add risk factor`` = 1m+decimal(90.0-gameState.``Max angle``)/500m
                                let ceRisk = 1m+decimal(90.0-gameState.``Max angle``)/2500m
                                let crashCount = snd(gameState.``Crash tick and count``)
                                printfn "Lap finished %s %s, max angle %f grip %f" car.Name car.Color gameState.``Max angle`` gameState.``Car setup``.``track grip factor``
                                let ``not first lap`` = msg.Data.Record.Value.RaceTime.IsSome && msg.Data.Record.Value.RaceTime.Value.Laps>1
                                match (gameState.``Max angle``< ``min previous lap angle`` && ``not first lap``)
                                    || (gameState.Laps = -1 && ``not first lap``), // Qualify (or bad race)
                                    (msg.Data.Record.Value.Ranking.IsSome && msg.Data.Record.Value.Ranking.Value.FastestLap > 1 && msg.Data.Record.Value.Ranking.Value.Overall > 1) with // Oh no! I'm losing! What should we do now!
                                // Avoid adding more risk if not crashed, in a couple of laps:
                                | true, true when (crashCount<1 || msg.GameTick.Value - 2*msg.Data.Record.Value.LapTime.Value.Ticks > fst(gameState.``Crash tick and count``) ) && gameState.``Max angle`` <= ``min previous lap angle`` -> 
                                    let ``drive more risky`` = gameState.``Car setup``.``track grip factor`` * ``add risk factor``
                                    let ``ce increase`` = gameState.``Car setup``.``allowed centrifugal force when curve exit`` * ceRisk
                                    let newSetup = { gameState.``Car setup`` with ``track grip factor`` = ``drive more risky``; ``allowed centrifugal force when curve exit`` = ``ce increase`` } 
                                    printfn "Car %s %s tries to run faster... new grip: %f, ce %f" car.Name car.Color ``drive more risky`` ``ce increase``
                                    send (ping) writer
                                    { gameState with ``Car setup`` = newSetup }
                                | false, _ when gameState.``Max angle`` >= ``max previous lap angle`` - (float crashCount) -> 
                                    let ``drive more safe`` = gameState.``Car setup``.``track grip factor`` / ``add risk factor``
                                    let ``ce decrease`` = gameState.``Car setup``.``allowed centrifugal force when curve exit`` / ceRisk
                                    let newSetup = { gameState.``Car setup`` with ``track grip factor`` = ``drive more safe``; ``allowed centrifugal force when curve exit`` = ``ce decrease`` } 
                                    printfn "Car %s %s tries to run safer... new grip: %f, ce %f" car.Name car.Color ``drive more safe`` ``ce decrease``
                                    send (ping) writer
                                    { gameState with ``Car setup`` = newSetup; ``Max angle``=0.0 }
                                | _ -> send (ping) writer; gameState
                        | "join" -> printfn "Joined"; send (ping) writer; gameState
                        | "gameInit" -> // Detect track and car setup
                            let trackname = msg.Data.Record.Value.Race.Value.Track.Name
                            let trackId = msg.Data.Record.Value.Race.Value.Track.Id
                            let crashCount = snd(gameState.``Crash tick and count``)
                            let carSetup, angle = // Don't reset the setup if already raced here...
                                let ``this track setup`` = match setup with Some(s) -> s | None -> ``get car setup by name`` trackId
                                let ``add risk factor`` = 1m+decimal(90.0-gameState.``Max angle``)/500m
                                let ceRisk = 1m+decimal(90.0-gameState.``Max angle``)/2500m
                                match trackId = gameState.``Track Id`` with
                                | false -> ``this track setup``,gameState.``Max angle`` //new track
                                | true when gameState.``Max angle`` >= ``max previous lap angle``- (float crashCount) || crashCount >1 -> 
                                    //Existing track... but added a lot of risk in qualify (/previous race)...
                                    let ``smaller risk`` = gameState.``Car setup``.``track grip factor`` / ``add risk factor``
                                    let ``ce decrease`` = gameState.``Car setup``.``allowed centrifugal force when curve exit`` / ceRisk
                                    { gameState.``Car setup`` with ``track grip factor`` =  ``smaller risk``; ``allowed centrifugal force when curve exit`` = ``ce decrease`` }, 0.0
                                | true -> gameState.``Car setup``,gameState.``Max angle`` //continue with same setup...

                            let laps = match msg.Data.Record.Value.Race.Value.RaceSession.Laps with
                                       | Some v -> v
                                       | None -> -1
                            
                            let track = msg.Data.Record.Value.Race.Value.Track.Pieces
                            let minRad, angles, clockwise = ``analyse track profile`` track

                            printfn "Race init %s with grip: %f" trackname carSetup.``track grip factor``; send (ping) writer
                            {gameState with ``Track Id`` = trackId; 
                                            ``Track shape`` = track,minRad,angles,clockwise;
                                            Lanes = msg.Data.Record.Value.Race.Value.Track.Lanes;
                                            Laps=laps;
                                            ``Car setup`` = carSetup;
                                            ``Crash tick and count`` = 0,0;
                                            ``Max angle`` = angle;
                                            ``Car size`` = msg.Data.Record.Value.Race.Value.Cars.[0].Dimensions.Length}
                        | "gameEnd" -> // Just some reporting of results
                            let trackName = match gameType with QuickRace -> "" | CreateRace(t,_) | JoinRace(t,_) -> t
                            let file = logPath + @"endGameData-" + trackName + ".log"
                            let acf = gameState.``Car setup``.``allowed centrifugal force when curve exit``
                            let custom = "\r\nParameters: " + trackName + " " +
                                            " f " + gameState.``Car setup``.``track grip factor``.ToString() +
                                            " acf " + acf.ToString() + "\r\n"
                            logResults custom trackName                       
                            logResults line trackName
                            logResults ",\n" trackName

                            let msg = JsonGet.Parse(line)
                            send (ping) writer
                            try
                                let racetime = msg.Data.Record.Value.Results.[0].Result.Millis
                                let laptime = msg.Data.Record.Value.BestLaps.[0].Result.Millis
                                printfn "Race ends %s, best lap %i, race %i" trackName laptime racetime
                                {gameState with ``Fastest lap`` = laptime; ``Race time`` = racetime }
                            with
                            | ex -> 
                                printfn "Race ends to DNF" 
                                {gameState with ``Fastest lap`` = maxTime; ``Race time`` = maxTime }
                        | "gameStart" -> printfn "Race starts"; send (ping) writer ; gameState
                        | "turboAvailable" -> 
                            let factor = msg.Data.Record.Value.TurboFactor.Value
                            let duration = msg.Data.Record.Value.TurboDurationTicks.Value
                            printfn "Got the turbo (d %i f %i)" duration factor; send (ping) writer ; 
                            let turbos = (*gameState.``Turbo available`` @*) [(duration, factor)] //doesn't stack
                            {gameState with ``Turbo available`` = turbos }
                        | "yourCar" -> // Identify my car!
                            printfn "Got a car..."
                            myCars.Clear()
                            myCars.Add(msg.Data.Record.Value.Name.Value + "-" + msg.Data.Record.Value.Color.Value)
                            send (ping) writer ; gameState
                        | "crash" when myCars.Contains(msg.Data.Record.Value.Name.Value + "-" + msg.Data.Record.Value.Color.Value) -> 
                            printfn "Crash... %s %s grip: %f" msg.Data.Record.Value.Name.Value msg.Data.Record.Value.Color.Value gameState.``Car setup``.``track grip factor``
                            // Oh no! Out of track! What should we do now!
                            let crashCount = snd(gameState.``Crash tick and count``)+1
                            let ``add more grip`` = gameState.``Car setup``.``track grip factor`` * (abs(8m - (decimal crashCount))/10m+0.1m)
                            let ``ce decrease`` = gameState.``Car setup``.``allowed centrifugal force when curve exit``  * (abs(8m - (decimal crashCount))/10m+0.1m)
                            printfn "After crash %i: %f, %f" crashCount ``add more grip`` ``ce decrease``
                            let newSetup = { gameState.``Car setup`` with ``track grip factor`` = ``add more grip``; ``allowed centrifugal force when curve exit``=``ce decrease`` } 
                            send (ping) writer
                            { gameState with ``Car setup`` = newSetup; ``Crash tick and count`` = msg.GameTick.Value,crashCount; ``Max angle``=0.0 }
                        | _ -> send (ping) writer ; gameState
                    with
                    | ex -> 
                        printfn "Exception: %s" (ex.ToString())
                        return gameState
            }

    let rec gameMainLoop (gameState:InGameData) =
        async {
            if gameState.``Race on-going`` |> not then return gameState 
            else
            let! res = 
                let networkData = 
                    try reader.ReadLineAsync() |> Async.AwaitTask
                    with | :? System.Net.Sockets.SocketException as se -> 
                        printfn "Network error but we retry once: %s " se.Message
                        reader.ReadLineAsync() |> Async.AwaitTask
                processNextLine networkData gameState
            return! gameMainLoop(res)
        }
    let initial = { ``Track Id``=""; ``Fastest lap`` = maxTime; ``Race time`` = maxTime; ``Track shape`` = [||],maxRadius,90m,true; ``Lanes`` = [||]; 
                    ``Race on-going`` = true; ``Car position`` = None; ``Turbo available`` = []; 
                    ``Laps`` = 999; ``Lane change request`` = SwitchInitialState; ``Car setup`` = DefaultRaceSetup; ``Crash tick and count`` = 0,0; ``Max angle`` = 0.0; ``Car size`` = 40}

    gameMainLoop initial
    |> Async.RunSynchronously

let race = ``race with given setup`` None

[<EntryPoint>]
let main args = 
    race QuickRace args |> ignore
    0